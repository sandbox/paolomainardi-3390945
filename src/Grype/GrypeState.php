<?php

namespace Drupal\drubom\Grype;

use Drupal\Core\State\StateInterface;
use Drupal\drubom\Store\VulnerabilitiesStore;
use Drupal\file\FileInterface;

/**
 * Represents the Grype state.
 */
class GrypeState {
  /**
   * The metadata.
   *
   * @var mixed
   */
  private $metadata;

  /**
   * The files.
   *
   * @var \Drupal\drubom\Grype\GrypeFile[]
   */
  private array $files = [];

  /**
   * Constructor.
   *
   * @param \Drupal\Core\State\StateInterface $drupalState
   *   The drupal state service.
   * @param \Drupal\drubom\Store\VulnerabilitiesStore $vulnerabilityStore
   *   The vulnerability store service.
   */
  public function __construct(
        private readonly StateInterface $drupalState,
        private readonly VulnerabilitiesStore $vulnerabilityStore
    ) {
    $this->metadata = $this->drupalState->get('drubom.grype.state.metadata');
    $this->files = $this->drupalState->get('drubom.grype.state.files') ?? [];
  }

  /**
   * Check if the state is empty.
   *
   * @return bool
   *   TRUE if the state is empty, FALSE otherwise.
   */
  public function isEmpty() {
    if (empty($this->metadata) || empty($this->files)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Set the metadata.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file containing the metadata.
   */
  public function setMetadata(FileInterface $file) {
    $this->metadata = (array) json_decode(file_get_contents($file->uri));
  }

  /**
   * Get the metadata.
   *
   * @return mixed
   *   The metadata.
   */
  public function getMetadata() {
    if (empty($this->metadata)) {
      throw new \Exception('Metadata not found in the state.');
    }

    return $this->metadata;
  }

  /**
   * Add a file to the state.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file to add.
   * @param string $drupalPath
   *   The drupal path of the file.
   *
   * @return \Drupal\drubom\Grype\GrypeFile
   *   The file object.
   */
  public function addFile(FileInterface $file, string $drupalPath): GrypeFile {
    $file = new GrypeFile($file, $drupalPath);
    $this->files[] = $file;

    return $file;
  }

  /**
   * Get the files.
   *
   * @return \Drupal\drubom\Grype\GrypeFile[]
   *   The files.
   */
  public function getFiles(): array {
    $not_found = [];
    foreach ($this->files as $file) {
      if (!file_exists($file->getDrupalPath())) {
        $not_found[] = $file->getDrupalPath();
      }
    }
    if (!empty($not_found)) {
      throw new \Exception(sprintf('Files "%s" not found', implode(', ', $not_found)));
    }

    return $this->files;
  }

  /**
   * Save the Drupal state.
   */
  public function drupalCommit() {
    $this->drupalState->set('drubom.grype.state.metadata', $this->getMetadata());
    $this->drupalState->set('drubom.grype.state.files', $this->getFiles());
  }

  /**
   * Get the cached copy files.
   *
   * @return string
   *   The cached copy files.
   */
  public function getCachedCopyFilesPath(): string|null {
    return $this->drupalState->get('drubom.grype.cached_files_path');
  }

  /**
   * Set the cached copy files.
   *
   * @param string $path
   *   The path to the cached copy files.
   */
  public function setCachedCopyFilesPath($path): void {
    $this->drupalState->set('drubom.grype.cached_files_path', $path);
  }

  /**
   * Sve the sbom.
   *
   * @param string $sbom
   *   The sbom to save.
   */
  public function saveSbom($sbom): void {
    $this->drupalState->set('drubom.grype.sbom', $sbom);
  }

  /**
   * Get the sbom.
   *
   * @return array|null
   *   The sbom document.
   */
  public function getSbomContainer(): array|null {
    return $this->drupalState->get('drubom.grype.sbom');
  }

  /**
   * Clear the state.
   */
  public function clear() {
    $this->drupalState->delete('drubom.grype.state.metadata');
    $this->drupalState->delete('drubom.grype.state.files');
    $this->drupalState->delete('drubom.grype.cached_files_path');
    $this->drupalState->delete('drubom.grype.sbom');
    foreach ($this->files as $file) {
      $file->delete();
    }
    $this->files = [];
    $this->metadata = [];
  }

  /**
   * Get the vulnerability store.
   *
   * @return \Drupal\drubom\Store\VulnerabilityStore
   *   The vulnerability store.
   */
  public function getStore() {
    $this->vulnerabilityStore->setContainer($this->getSbomContainer());

    return $this->vulnerabilityStore;
  }

}
