<?php

namespace Drupal\drubom\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\drubom\DrubomExecManager;
use Drupal\drubom\Grype\GrypeState;
use Drupal\drubom\SyftController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a drubom form.
 */
final class ReportForm extends FormBase {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\drubom\DrubomExecManager $drubomExecManager
   *   The DrubomExecManager service.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The config factory service.
   * @param \Drupal\drubom\SyftController $syftController
   *   The SyftController service.
   * @param \Drupal\drubom\Grype\GrypeState $grypeState
   *   The GrypeState service.
   */
  final public function __construct(
    private readonly StateInterface $state,
    private readonly DrubomExecManager $drubomExecManager,
    private readonly ConfigFactory $config,
    private readonly SyftController $syftController,
    private readonly GrypeState $grypeState,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('state'),
      $container->get('drubom.exec_manager'),
      $container->get('config.factory'),
      $container->get('drubom.syft_controller'),
      $container->get('drubom.grype_state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drubom_report';
  }

  /**
   * Extract packages by language.
   *
   * @param object $sbomObject
   *   The sbom object.
   * @param string $sbomFormat
   *   The sbom format.
   *
   * @return array
   *   The packages.
   */
  private function extractPackagesByLanguage($sbomObject, $sbomFormat) {
    $packages = [];
    if ($sbomFormat !== 'cyclonedx-json') {
      return [];
    }
    if (empty($sbomObject->components)) {
      return [];
    }
    foreach ($sbomObject->components as $package) {
      $type = '';
      foreach ($package->properties as $property) {
        if ($property->name == 'syft:package:type') {
          $type = $property->value;
          break;
        }
      }
      if (!empty($type)) {
        $packages[$type][] = [
          'title' => $package->name,
          'syft_package_type' => $package->properties[2]->value,
        ];
      }
    }
    return $packages;
  }

  /**
   * Generate data form components.
   *
   * @param array $syftState
   *   The syft state.
   * @param string $sbomFormat
   *   The sbom format.
   * @param array $form
   *   The form.
   */
  private function generateDataFormComponents(array $syftState, string $sbomFormat, array &$form) {
    $form['sbom']['packages']['#prefix'] = '<h2>' . $this->t('SBOM') . '</h2>';
    $form['sbom']['packages']['timestamp'] = [
      '#markup' => empty($syftState['timestamp']) && empty($syftState['data'])
        ? '<p>' . $this->t('Not yet generated.') . '</p>'
        : '<p>' . $this->t('Generated on: %date', ['%date' => date('Y-m-d - H:i:s', $syftState['timestamp'])]) . '</p>',
    ];

    // Add some default actions.
    $binary = $this->syftController->getSyftBinary();
    if (empty($binary)) {
      $this->messenger()->addError($this->t(
        'Syft binary not found. Please configure the path to the Syft binary in the <a href=":url"> settings page </a>',
        [
          ':url' => Url::fromRoute('drubom.settings_form')->toString(),
        ]
      ));
    }
    $form['actions']['generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate SBOM'),
      '#weight' => 50,
      '#submit' => ['::generateSbomForm'],
      '#attributes' => !empty($binary) ? [] : ['disabled' => 'disabled'],
    ];

    if (empty($syftState) && empty($syftState['data'])) {
      return;
    }

    if ($sbomFormat !== 'cyclonedx-json') {
      $this->messenger()->addWarning($this->t('Cannot generate data summary report for format %format', ['%format' => $sbomFormat]));
      return;
    }

    $sbomObject = json_decode($syftState['data']);
    if (json_last_error() !== JSON_ERROR_NONE) {
      $this->messenger()->addError($this->t('Error decoding SBOM: %error', ['%error' => json_last_error_msg()]));
      return;
    }
    $packagesByLanguage = $this->extractPackagesByLanguage($sbomObject, $sbomFormat);
    $headers = [
      'title' => [
        'data' => $this->t('Package type'),
      ],
      'syft_package_type' => $this->t('Number'),
    ];

    $packagesCount = [];
    foreach ($packagesByLanguage as $lang => $packages) {
      $packagesCount[] = [
        'title' => $lang,
        'syft_package_type' => count($packages),
      ];
    }
    $form['sbom']['packages']['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $packagesCount,
    ];
    if (!empty($packagesCount)) {
      $form['sbom']['packages']['raw_json'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Raw JSON'),
        '#default_value' => json_encode($sbomObject, JSON_PRETTY_PRINT),
        '#rows' => 10,
        '#attributes' => [
          'readonly' => 'readonly',
        ],
      ];

      $form['actions']['download_sbom'] = [
        '#type' => 'submit',
        '#value' => $this->t('Download SBOM'),
        '#weight' => 100,
        '#validate' => ['::validateDownloadForm'],
        '#submit' => ['::downloadSbomForm'],
      ];

      $form['sbom']['packages']['#suffix'] = '<p>' . $this->t('Total packages: <strong> %number </strong>', ['%number' => count($sbomObject->components)]) . '</p>';
    }
  }

  /**
   * Generate vulnerabilities form components.
   *
   * @param array $sbomContainer
   *   The sbom container.
   * @param string $sbomFormat
   *   The sbom format.
   * @param array $form
   *   The form.
   */
  private function generateVulnerabilitiesFormComponents(array $sbomContainer, string $sbomFormat, array &$form) {
    try {
      if ($this->grypeState->isEmpty() || empty($sbomContainer['data'])) {
        return;
      }
      if ($sbomFormat !== 'cyclonedx-json') {
        $this->messenger()->addWarning($this->t('Cannot generate data summary report for format %format', ['%format' => $sbomFormat]));
        return;
      }
      $store = $this->grypeState->getStore();

      // Print vulnerabilities table.
      $form['sbom']['vulns']['#prefix'] = '<h2>' . $this->t('Vulnerabilities') . '</h2>';

      $vulns = $store->getVulnerabilities();
      if (empty($vulns)) {
        $form['sbom']['vulns']['#suffix'] = strtr('<p>%message</p>', ['%message' => $this->t('No vulnerabilities found.')]);
        return;
      }

      $table = $store->getVulnsTable();
      $form['sbom']['vulns']['table'] = [
        '#type' => 'table',
        '#header' => $table['headers'],
        '#rows' => $table['rows'],
      ];
      if (!empty($table['rows'])) {
        $form['sbom']['vulns']['#suffix'] = '<p>' . $this->t('Total vulnerabilities: <strong> %number </strong>', ['%number' => count($table['rows'],)]) . '</p>';
        $form['actions']['download_vulns'] = [
          '#type' => 'submit',
          '#value' => $this->t('Download Vulnerability SBOM'),
          '#weight' => 100,
          '#validate' => ['::validateDownloadVulnsSbomForm'],
          '#submit' => ['::downloadVulnsSbomForm'],
        ];
      }
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateDownloadVulnsSbomForm($form, FormStateInterface $form_state) {
    try {
      $this->grypeState->getStore()->getSbom();
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('message', $e->getMessage());
    }
  }

  /**
   * Download vulnerability SBOM.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function downloadVulnsSbomForm(array $form, FormStateInterface $form_state) {
    $sbom = $this->grypeState->getStore()->getSbomAsJson();
    $this->download($form_state, $sbom, 'vulns');
  }

  /**
   * Download SBOM.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function downloadSbomForm(array $form, FormStateInterface $form_state) {
    $sbom = $this->state->get('drubom.sbom');
    $this->download($form_state, $sbom['data']);
  }

  /**
   * Download the sbom file.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string $data
   *   The data.
   * @param string $prefix
   *   The prefix.
   */
  private function download(FormStateInterface $form_state, $data, $prefix = "") {
    if (empty($data)) {
      $this->messenger()->addError($this->t('Data is empty, cannot be downloaded.'));
      $form_state->setRedirect('drubom.report');
      return;
    }
    $response = new JsonResponse();
    $disposition = HeaderUtils::makeDisposition(
      HeaderUtils::DISPOSITION_ATTACHMENT,
      $this->drubomExecManager->filename($prefix)
    );
    $response->setContent($data);
    $response->headers->set('Content-Type', 'application/json');
    $response->headers->set('Content-Disposition', $disposition);
    $response->setStatusCode(200);
    $form_state->setResponse($response);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    try {

      // Get the saved SBOM from the database if exists.
      $syftState = $this->state->get('drubom.sbom', []);

      // Get the vulnerabilities from the database if exists.
      $sbomContainer = $this->grypeState->getSbomContainer();

      // Define actions that can be added to the form.
      $form['actions'] = [
        '#type' => 'actions',
      ];

      // Decode sbom.
      $sbomFormat = $this->config('drubom.settings')->get('sbom_output_format');

      // SBOM report data components.
      $this->generateDataFormComponents($syftState, $sbomFormat, $form);
      $this->generateVulnerabilitiesFormComponents($sbomContainer, $sbomFormat, $form);
      return $form;
    }
    catch (\Exception | \TypeError $e) {
      $this->messenger()->addError($e->getMessage());
      return $form;
    }
  }

  /**
   * Generate SBOM.
   */
  public function generateSbomForm() {
    try {
      $this->syftController->run();
    }
    catch (\Exception $e) {
      $this->messenger()->addError($this->t('Error generating SBOM: %error', ['%error' => $e->getMessage()]));
    }
  }

  /**
   * Validate download form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateDownloadForm(array $form, FormStateInterface $form_state) {
    $sbom = $this->state->get('drubom.sbom');
    if (empty($sbom['data'])) {
      $form_state->setErrorByName('message', 'SBOM not yet generated.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}
