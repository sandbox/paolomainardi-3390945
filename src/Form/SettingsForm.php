<?php

namespace Drupal\drubom\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\drubom\DrubomExecManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure drubom settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The typed config manager.
   *
   * @var \Drupal\Core\Config\TypedConfigManagerInterface
   */
  protected $typedConfigManager;

  /**
   * The Link generator Service.
   *
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $link;

  /**
   * Constructs a new SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The Link Generator service.
   * @param \Drupal\drubom\DrubomExecManager $drubomExecManager
   *   Drubom execManager.
   */
  final public function __construct(
    ConfigFactoryInterface $configFactory,
    TypedConfigManagerInterface $typedConfigManager,
    LinkGeneratorInterface $link_generator,
    private readonly DrubomExecManager $drubomExecManager
  ) {
    parent::__construct($configFactory, $typedConfigManager);
    $this->link = $link_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('link_generator'),
      $container->get('drubom.exec_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'drubom_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['drubom.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['sbom_output_format'] = [
      '#type' => 'select',
      '#title' => $this->t('SBOM output format'),
      '#description' => $this->t('The SBOM format.'),
      '#options' => [
        'cyclonedx-json' => $this->t('CycloneDX JSON'),
        'spdx-json' => $this->t('SPDX JSON'),
      ],
      '#default_value' => $this->config('drubom.settings')->get('sbom_output_format'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('drubom.settings')
      ->set('sbom_output_format', $form_state->getValue('sbom_output_format'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
