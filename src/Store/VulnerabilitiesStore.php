<?php

namespace Drupal\drubom\Store;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Vulnerabilities store.
 */
class VulnerabilitiesStore {
  use \Drupal\Core\StringTranslation\StringTranslationTrait;

  /**
   * The sbom container.
   *
   * @var array
   *  The sbom container.
   */
  private array $sbomContainer = [];

  /**
   * Constructor.
   */
  public function __construct() {
  }

  /**
   * Get the store.
   */
  public function setContainer(array $sbomContainer) {
    $this->sbomContainer = $sbomContainer;
  }

  /**
   * Get the sbom object.
   *
   * @return object
   *   The sbom object.
   */
  public function getSbom(): object {
    $sbom = $this->sbomContainer;
    if (empty($sbom['data'])) {
      throw new \Exception('Vulnerabilities SBOM not found in the state.');
    }
    // Decode the JSON.
    $sbom = json_decode($sbom['data']);
    if (json_last_error() !== JSON_ERROR_NONE) {
      throw new \Exception('Vulnerabilities SBOM invalid JSON: ' . json_last_error_msg());
    }

    return $sbom;
  }

  /**
   * Get the sbom as JSON.
   *
   * @return string
   *   The sbom as JSON.
   */
  public function getSbomAsJson(): string {
    return json_encode($this->getSbom(), JSON_PRETTY_PRINT);
  }

  /**
   * Get the vulnerabilities.
   *
   * @return array
   *   The list of vulnerabilities.
   */
  public function getVulnerabilities() {
    $sbom = $this->getSbom();
    $vulns = $sbom->vulnerabilities ?? [];
    if (empty($vulns)) {
      return [];
    }

    return $vulns;
  }

  /**
   * Get the advisory link based on the type of vulnerability.
   *
   * @return \Drupal\Core\Link
   *   The public advisor link based on type.
   */
  private function getAdvisoryLink($vuln) {
    switch ($vuln) {
      case str_starts_with($vuln->id, 'GHSA'):
        $link = Link::fromTextAndUrl($vuln->id, Url::fromUri(strtr('https://github.com/advisories/%vuln', ['%vuln' => $vuln->id])));
        break;

      case str_starts_with($vuln->id, 'CVE'):
        $link = Link::fromTextAndUrl($vuln->id, Url::fromUri(strtr('https://cve.mitre.org/cgi-bin/cvename.cgi?name=%vuln', ['%vuln' => $vuln->id])));
        break;

      default:
        $link = Link::fromTextAndUrl($vuln->id, Url::fromUri(strtr('https://nvd.nist.gov/vuln/detail/%vuln', ['%vuln' => $vuln->id])));
        break;
    }

    return $link;
  }

  /**
   * Get the vulnerabilities table.
   *
   * @return array
   *   The vulnerabilities formatted as a table.
   */
  public function getVulnsTable() {
    $vulns = $this->getVulnerabilities();
    if (empty($vulns)) {
      return [];
    }
    $headers = [
      'id' => $this->t('ID'),
      'source' => $this->t('Source'),
      'severity' => $this->t('Severity'),
      'affects' => $this->t('Affects'),
    ];
    foreach ($vulns as $vuln) {
      $ratings = array_pop($vuln->ratings);
      $affects = [];
      foreach ($vuln->affects as $affect) {
        $affects[] = $affect->ref;
      }
      $link = $this->getAdvisoryLink($vuln);
      $rows[] = [
        'id' => $link->toString(),
        'source' => $vuln->source->name,
        'severity' => strtr('%severity (%score)', [
          '%severity' => $ratings->severity,
          '%score' => $ratings->score ?? 'N/A',
        ]
        ),
        'affects' => implode(', ', $affects),
      ];
    }
    // Sort rows by severity.
    usort($rows, function ($a, $b) {
        return $a['severity'] <=> $b['severity'];
    });

    return [
      'headers' => $headers,
      'rows' => $rows,
    ];
  }

}
